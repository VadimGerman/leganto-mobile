import 'package:api_sdk/rest/api_helpers/api_base_helper.dart';

class RestApiHandlerData {
  static ApiBaseHelper _apiBaseHelper = ApiBaseHelper();
  static getData(String path, {Map<String, String> headers, String params}) async {
    final response = await _apiBaseHelper.get(path, headers: headers, params: params);
    return response;
  }

  static postData(String path, {Map<String, String> headers, dynamic body}) async {
    final response = await _apiBaseHelper.post(path, body: body, headers: headers);
    return response;
  }

  static putData(String path, {Map<String, String> headers, dynamic body}) async {
    final response = await _apiBaseHelper.put(path, headers, body);
    return response;
  }

  static deleteData(String path, {Map<String, String> headers}) async {
    final response = await _apiBaseHelper.delete(path, headers);
    return response;
  }
}
