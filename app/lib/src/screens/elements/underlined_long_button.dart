
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UnderlinedLongButton extends StatelessWidget {
  UnderlinedLongButton({required this.titleWidget,
    required this.functionalBlock,
    required this.functionalBlockWidth,
    required this.height,
    this.decorationLeftIcon = Icons.folder_outlined,
    this.leftIconAllignment = Alignment.center,
    this.isDecorationIconVisible = false,
    this.isUsePadding = true
    });

  final Widget titleWidget;
  final Widget functionalBlock;
  final double functionalBlockWidth;
  final double height;
  final IconData decorationLeftIcon;
  final Alignment leftIconAllignment;
  final bool isDecorationIconVisible;
  final bool isUsePadding;

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var bigTextSize = allWidth * 0.055;
    var buttonWidth = allWidth * 0.94;
    var decorationPartWidth = isDecorationIconVisible ? allWidth * 0.12 : 0.0;
    var textPartWidth = buttonWidth - decorationPartWidth -
        functionalBlockWidth - allWidth * 0.03;

    return Container(
      width: allWidth,
      color: Color(0xffffffff),
      child: Align(
          alignment: Alignment.topRight,
          child: Container(
            width: buttonWidth,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.5, color: Color(0xffdadada)),
              ),
            ),

            child: Container(
              padding: isUsePadding ? EdgeInsets.fromLTRB(0, 8, 8, 8) : EdgeInsets.zero,
              child: Row(
                children: [
                  Visibility(
                    visible: isDecorationIconVisible,
                    child: Container(
                      width: decorationPartWidth,
                      height: this.height,
                      child: Align(
                        alignment: leftIconAllignment,
                        child: Icon(
                          Icons.folder_outlined,
                          size: bigTextSize,
                          color: Color(0xff808486),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: textPartWidth,
                    height: this.height,
                    child: titleWidget,
                  ),
                  functionalBlock,
                ],
              )
            )
          )
      )
    );
  }



}
