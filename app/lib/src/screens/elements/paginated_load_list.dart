import 'package:overone/src/screens/elements/paginated_load/paginated_item_base.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_bloc_public.dart';

class PaginatedLoadList extends StatefulWidget {
  PaginatedLoadList({Key? key, required this.item}) : super(key: key) {
  }

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  PaginatedItemBase item;

  @override
  _PaginatedListState createState() => _PaginatedListState(item: item);
}

class _PaginatedListState extends State<PaginatedLoadList>
  with AutomaticKeepAliveClientMixin<PaginatedLoadList>{
  _PaginatedListState({required this.item});

  PaginatedItemBase item;

  @override
  void initState() {
    super.initState();
    this.item.loadData();
  }

  Future<void> _pullRefresh() async {
    item.refresh();
  }

  Container getContentPage(double allWidth) {
    var textSize = allWidth * 0.034;
    var iconSize = allWidth * 0.038;
    final ButtonStyle style =
    ElevatedButton.styleFrom(
      textStyle: const TextStyle(fontSize: 20),
      primary: Color(0xffe0f0f2),
      elevation: 1,
    );

    if (!this.item.isLoading && this.item.items.isEmpty) {
      return Container(
        child: Center(
          child: Text(
            this.item is PaginatedNewsItem ?
              "Здесь пока нет ни одной новости" :
              "Здесь пока нет ни одного источника"
          )
        )
      );
    }

    return Container(
      child: Column (
        children: <Widget>[
          Visibility(
            visible: this.widget.item.hasUpdates,
            child: Container(
              width: allWidth * 0.34,
              child: ElevatedButton(
                style: style,
                onPressed: _pullRefresh,
                child: Center(
                  child: Row(
                    children: [
                      Icon(
                        Icons.sync,
                        color: Color(0xff0a93a6),
                        size: iconSize,
                      ),
                      SizedBox(width: allWidth * 0.027),
                      Text(
                        'ОБНОВИТЬ',
                        style: TextStyle(
                          color: Color(0xff0a93a6),
                          fontSize: textSize,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Helvetica Neue',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Expanded(
              child: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if (!this.item.isLoading && scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                    this.item.loadData();
                  }

                  return false;
                },
                child: ListView.builder(
                  itemCount: (this.item.items.length == null) ? 0 : this.item.items.length,
                  itemBuilder: (context, index) => this.item.getViewItem(index),
                ),
              ),
            ),
          ),
          Container(
            height: this.item.isLoading ? 70.0 : 0,
            color: Colors.transparent,
            child: Center(
              child: new CircularProgressIndicator(),
            ),
          ),
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: () async => false,
      child: RefreshIndicator(
        onRefresh: _pullRefresh,
        child: BlocBuilder<ContentBloc, ContentState>(
          cubit: this.item.contentBloc,
          builder: (BuildContext context, ContentState state) {
            this.item.updateState(state);

            return this.getContentPage(allWidth);
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
