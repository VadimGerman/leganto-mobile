
import 'package:flutter/material.dart';
import 'package:overone/src/screens/home/folders/folder_selection.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_event.dart';

class SubscribeButton extends StatefulWidget {
  SubscribeButton({
    required this.contentBloc,
    required this.isSubscribed,
    required this.feedId,
    this.folderId = -1,
  });

  ContentBloc contentBloc;
  bool isSubscribed;
  final int feedId;
  final int folderId;

  @override
  State<SubscribeButton> createState() => _SubscribeButton();
}

class _SubscribeButton extends State<SubscribeButton> {
  _SubscribeButton();

  void unsubscribe(int folderId) {
    List<int> folderIds = folderId == -1 ? [] : [folderId];
    this.widget.contentBloc.add(
        SubscribeActions(
            doSubscribe: false,
            feedId: this.widget.feedId,
            folderIds: folderIds)
    );
  }

  void unsubscribeDialog(BuildContext context, int folderId) {
    showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          content: Text('Истончник останется в папке «Все подписки»'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('ОТМЕНИТЬ'),
            ),
            TextButton(
              onPressed: () {
                unsubscribe(this.widget.folderId);
                Navigator.pop(context);
              },
              child: Text('ОК'),
            ),
            TextButton(
              onPressed: () {
                unsubscribe(-1);
                Navigator.pop(context);
              },
              child: Text('Отписаться полностью'),
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;

    return IconButton(
      icon: Icon(
        widget.isSubscribed ? Icons.done : Icons.add,
        color: widget.isSubscribed
            ? Color(0xff0a93a6)
            : Color(0xff151515),
        size: allWidth * 0.065,
      ),
      onPressed: () {
        setState(() {
          if (widget.isSubscribed) {
            if (widget.folderId != -1) {
              unsubscribeDialog(context, -1);
            } else {
              unsubscribe(-1);
            }
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                new FolderSelection(feedId: widget.feedId),
              ),
            );
          }
        });
      },
    );
  }
}
