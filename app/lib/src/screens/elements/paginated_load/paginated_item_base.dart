import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/pagination_info.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/common/models/news_data.dart';
import 'package:shared/modules/common/models/feed_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';
import 'package:shared/modules/content/bloc/content/content_event.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

import '../feed_box.dart';
import '../news_box.dart';

abstract class PaginatedItemBase<T> {

  final ContentBloc contentBloc = ContentBlocController().contentBloc;
  final PaginationInfo info = PaginationInfo(limit: 4, offset: 0, sort: 0, order: 1);
  bool hasUpdates = false;
  bool isLoading = false;
  List<T> items = [];

  void updateState(ContentState state) {}

  void loadData();

  void refresh() {
    items.clear();
    info.offset = 0;
    loadData();
  }

  Widget getViewItem(int index);

  void _resetItems(List<T> newItems) {
    this.items = newItems;
    this.info.offset = 0;

    this.isLoading = false;
  }

  void _addItems(List<T> newItems) {
    this.items.addAll(newItems);
    this.info.offset += newItems.length;

    this.isLoading = false;
  }

  void _updateAt(int pos, T newItem) {
    this.items[pos] = newItem;

    this.isLoading = false;
  }
}

class PaginatedNewsItem extends PaginatedItemBase<NewsData> {

  PaginatedNewsItem({required this.listInfo});

  NewsListData listInfo;

  @override
  void updateState(ContentState state) {
    if (state is NewsBecome &&
        this.listInfo.type == ListTypes.mainlenta) {
      _addItems(state.news.items);
      this.hasUpdates = state.news.hasUpdates;
    } if (state is RecomendationsBecome &&
        this.listInfo.type == ListTypes.recomendations) {
      _addItems(state.news.items);
      this.hasUpdates = state.news.hasUpdates;
    } if (state is FeedNewsBecome &&
        this.listInfo.type == ListTypes.feedlenta &&
        this.listInfo.feedId == state.news.feedId) {
      _addItems(state.news.items);
      this.hasUpdates = state.news.hasUpdates;
    } if (state is UserListNewsBecome &&
        this.listInfo.type == ListTypes.userlist &&
        this.listInfo.listName == state.news.listName) {
      _addItems(state.news.items);
    } if (state is UserFolderNewsBecome &&
        this.listInfo.type == ListTypes.folderlenta &&
        this.listInfo.folderId == state.news.folderId) {
      _addItems(state.news.items);
    }  else if (state is ContentFailure) {
      _addItems([]);
    }
  }

  @override
  void loadData() {
    this.isLoading = true;

    if (this.listInfo.type == ListTypes.recomendations) {
      this.contentBloc.add(GetUserRecomendations(
          firstId: this.items.length > 0 ? this.items.first.id : -1,
          lastId: this.items.length > 0 ? this.items.last.id : -1,
          sort: this.info.sort,
          order: this.info.order));
    } else if (this.listInfo.type == ListTypes.feedlenta) {
      this.contentBloc.add(GetFeedNews(
          feedId: this.listInfo.feedId,
          firstId: this.items.length > 0 ? this.items.first.id : -1,
          lastId: this.items.length > 0 ? this.items.last.id : -1,
          sort: this.info.sort,
          order: this.info.order));
    } else if (this.listInfo.type == ListTypes.userlist) {
      this.contentBloc.add(GetUserListNews(
          listName: this.listInfo.listName,
          lastId: this.items.length > 0 ? this.items.last.id : -1,
          sort: this.info.sort,
          order: this.info.order));
    } else if (this.listInfo.type == ListTypes.mainlenta) {
      this.contentBloc.add(GetUserNews(
          firstId: this.items.length > 0 ? this.items.first.id : -1,
          lastId: this.items.length > 0 ? this.items.last.id : -1,
          sort: this.info.sort,
          order: this.info.order));
    } else if (this.listInfo.type == ListTypes.folderlenta) {
      this.contentBloc.add(GetFolderNews(
          folderId: this.listInfo.folderId,
          firstId: this.items.length > 0 ? this.items.first.id : -1,
          lastId: this.items.length > 0 ? this.items.last.id : -1,
          sort: this.info.sort,
          order: this.info.order));
    }
  }

  @override
  Widget getViewItem(int index) {
    return NewsBox(
        info: this.items[index],
        listName: this.listInfo.listName,
        key: Key("${index}"));
  }
}

class PaginatedFeedItem extends PaginatedItemBase<FeedData> {
  PaginatedFeedItem({required this.listInfo, this.folderId = -1});

  FeedListData listInfo;
  final int folderId;

  @override
  void updateState(ContentState state) {
    if (state is AllFeedsBecome &&
        listInfo.type == state.feeds.type) {
      if (listInfo.type == FeedListTypes.categoryfeeds &&
          listInfo.categoryId == state.feeds.categoryId) {
        _resetItems(state.feeds.items);
      } else if (listInfo.type == FeedListTypes.folderfeeds &&
          listInfo.folderId == state.feeds.folderId) {
        _resetItems(state.feeds.items);
      } else if (listInfo.type == FeedListTypes.alluserfeeds) {
        _resetItems(state.feeds.items);
      }
    } else if (state is SubscribeChanged) {
      var feed = state.singleFeed;
      if (feed.items.isNotEmpty) {
        var id = feed.items.first.id;
        var pos = this.items.indexWhere((element) => element.id == id);
        var updatedFeed = this.items[pos];
        updatedFeed.isSubscribed = feed.items.first.isSubscribed;

        _updateAt(pos, updatedFeed);
      }
    } else if (state is ContentFailure) {
      _addItems([]);
    }
  }

  @override
  void loadData() {
    this.isLoading = true;

    if (this.listInfo.type == FeedListTypes.folderfeeds) {
      this.contentBloc.add(GetFolderFeeds(
          folderId: this.listInfo.folderId));
    } else if (this.listInfo.type == FeedListTypes.categoryfeeds) {
      this.contentBloc.add(GetCategoryFeeds(
          categoryId: this.listInfo.categoryId,
          offset: this.info.offset,
          sort: this.info.sort,
          order: this.info.order));
    } else if (this.listInfo.type == FeedListTypes.alluserfeeds) {
      this.contentBloc.add(GetAllUserFeeds());
    }
  }

  @override
  Widget getViewItem(int index) {
    return FeedBox(info: this.items[index], folderId: this.folderId, key: Key("${index}"));
  }
}
