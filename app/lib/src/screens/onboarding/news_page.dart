
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsPage extends StatelessWidget {
  NewsPage({key, required this.path, required this.title}) : super(key: key);

  final String path;
  final String title;

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Color(0xfff4f4f4),
        elevation: 1,
        iconTheme: IconThemeData(
          color: Color(0xff151515),
        ),
        title: Text(title,
          style: TextStyle(
              color: Color(0xff151515),
              fontWeight: FontWeight.w500,
              fontFamily: 'Helvetica Neue',
              fontStyle: FontStyle.normal,
              fontSize: titleSize
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        )
      ),
      body: WebView(
        initialUrl: path,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}