
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';

import '../some_lenta.dart';

class ArticlesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ArticlesScreenState();
}

class _ArticlesScreenState extends State<ArticlesScreen>
    with SingleTickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    Tab(text: 'ЗАКЛАДКИ'),
    Tab(text: 'ИСТОРИЯ'),
  ];

  List<SomeLenta> pages = [];
  late TabController _tabController;

  @override
  void initState() {
    super.initState();

    pages = <SomeLenta>[
      SomeLenta(
        newsListInfo: NewsListData(
          type: ListTypes.userlist,
          listName: 'bookmarks_d3Sk9N',
        ),
        feedListInfo: FeedListData(
          type: FeedListTypes.none,
        ),
      ),
      SomeLenta(
        newsListInfo: NewsListData(
          type: ListTypes.userlist,
          listName: 'history_d3Sk9N',
        ),
        feedListInfo: FeedListData(
          type: FeedListTypes.none,
        ),
      ),
    ];

    _tabController = TabController(
        vsync: this,
        length: tabs.length,
        initialIndex: 0
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var textSize = allWidth * 0.033;
    var logoSize = allWidth * 0.055;
    var bigTextSize = allWidth * 0.055;

    return Scaffold(
      // bottomNavigationBar: BottomNavigationBarWidget(),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Color(0xfff4f4f4),
          elevation: 1,
          title: Text('Избранное',
            style: TextStyle(
              color: Color(0xff151515),
              fontWeight: FontWeight.w500,
              fontFamily: 'Helvetica Neue',
              fontStyle: FontStyle.normal,
              fontSize: logoSize
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(12),
            ),
          ),
          bottom: TabBar(
            indicatorColor: Color(0xff0A93A6),
            indicatorSize: TabBarIndicatorSize.label,
            labelColor: Color(0xff151515),
            unselectedLabelColor: Color(0xff808486),

            labelStyle: TextStyle(
                fontSize: textSize,
                fontWeight: FontWeight.w500,
                fontFamily: 'Helvetica Neue'),
            unselectedLabelStyle: TextStyle(
                fontSize: textSize,
                fontWeight: FontWeight.w500,
                fontFamily: 'Helvetica Neue'),
            controller: _tabController,
            tabs: tabs,
          ),
        ),
        body: Container(
          padding: EdgeInsets.zero,
          child: TabBarView(
            controller: _tabController,
            children: pages,
          ),
        )
    );
  }
}
