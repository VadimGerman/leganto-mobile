
import 'package:overone/src/screens/elements/paginated_load/paginated_item_base.dart';
import 'package:overone/src/screens/elements/paginated_load_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overone/src/screens/elements/subscribe_button.dart';
import 'package:shared/modules/common/models/feed_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

class SourceArticles extends StatefulWidget {
  const SourceArticles(this.info, this.folderId);

  final FeedData info;
  final int folderId;

  @override
  State<SourceArticles> createState() => _SourceArticlesState();
}

class _SourceArticlesState extends State<SourceArticles> {
  final ContentBloc contentBloc =
      ContentBlocController().contentBloc;

  Future<bool> onGoBack() async {
    Navigator.of(context).pop();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;

    if (this.widget.info == null) {
      return Container();
    }

    return WillPopScope(
      onWillPop: onGoBack,
      child: BlocBuilder<ContentBloc, ContentState>(
        cubit: contentBloc,
        builder: (BuildContext context, ContentState state) {
          if (state is SubscribeChanged) {
            var feed = state.singleFeed;
            if (feed.items.isEmpty) {
              // TODO: Catch this or do something else.
              throw "Unreachable";
            } else if (this.widget.info.id == feed.items.first.id) {
              // setState(() {
                this.widget.info.reset(feed.items.first);
              // });
            }
          }

          return Scaffold(
            // bottomNavigationBar: BottomNavigationBarWidget(),

              appBar: AppBar(
                automaticallyImplyLeading: true,
                backgroundColor: Color(0xfff4f4f4),
                elevation: 1,
                iconTheme: IconThemeData(
                  color: Color(0xff151515),
                ),
                title: Text(widget.info.title,
                  style: TextStyle(
                      color: Color(0xff151515),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Helvetica Neue',
                      fontStyle: FontStyle.normal,
                      fontSize: titleSize
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(12),
                  ),
                ),
                actions: <Widget>[
                  SubscribeButton(
                    contentBloc: contentBloc,
                    feedId: widget.info.id,
                    isSubscribed: widget.info.isSubscribed,
                    folderId: widget.folderId,
                  ),
                ],
              ),
              body: new PaginatedLoadList(
                  item: PaginatedNewsItem(
                    listInfo: NewsListData(
                      type: ListTypes.feedlenta,
                      feedId: widget.info.id,
                    )
                  )
              )
          );
        }
      )
    );
  }
}
