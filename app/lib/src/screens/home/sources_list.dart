
import 'package:overone/src/screens/elements/paginated_load/paginated_item_base.dart';
import 'package:overone/src/screens/elements/paginated_load_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/category.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';

// TODO: This should work for top level categories too. Implement on server side.
class CategorySourcesList extends StatelessWidget {
  CategorySourcesList({required this.category});

  final CategoryInfo category;

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: true,
              iconTheme: IconThemeData(
                color: Color(0xff151515),
              ),
              backgroundColor: Color(0xfff4f4f4),
              elevation: 1,
              title: Text(
                category.title,
                style: TextStyle(
                    color: Color(0xff151515),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Helvetica Neue',
                    fontStyle: FontStyle.normal,
                    fontSize: titleSize
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(12),
                ),
              )
          ),
          body: new PaginatedLoadList(item: PaginatedFeedItem(
              listInfo: FeedListData(
                  items: [],
                  type: FeedListTypes.categoryfeeds,
                  folderId: -1,
                  categoryId: category.id))
          )
      ),
    );
  }
}
