
import 'package:overone/src/screens/home/folders/create_edit_folder.dart';
import 'package:overone/src/screens/elements/underlined_long_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/folder_info.dart';
import 'package:shared/modules/common/models/news_list_data.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_bloc_public.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

import 'folder_screen.dart';

class FoldersList extends StatefulWidget {
  final ContentBloc contentBloc = ContentBlocController().contentBloc;
  List<FolderInfo> items = [];

  @override
  State<FoldersList> createState() => _FoldersListState();
}

class _FoldersListState extends State<FoldersList> {
  int _allFeedsCount = 0;
  bool isEditing = false;

  @override
  void initState() {
    super.initState();
    this.widget.contentBloc.add(GetFolders());
  }

  void updateState(List<FolderInfo> folders, int allFeedsCount) {
    // TODO: Some states emits multiple times. That's not good, figure out what the deal.
    var set = Set.from(folders);
    folders = List.from(set);
    folders.sort((a, b) => a.folderPosition.compareTo(b.folderPosition));
    widget.items = folders;
    _allFeedsCount = allFeedsCount;
  }

  UnderlinedLongButton getLongButton(IconData iconData, String title, 
      int feedsCount, double functionalBlockWidth, Widget functionalBlock) {
    var allWidth = MediaQuery.of(context).size.width;
    var mainTextSize = allWidth * 0.038;
    var smallTextSize = allWidth * 0.030;

    return UnderlinedLongButton(
      decorationLeftIcon: iconData,
      isDecorationIconVisible: true,
      titleWidget: Column(
          children: [
            SizedBox(height: allWidth * 0.022),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  title,
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: mainTextSize,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff151515),
                  ),
                  maxLines: 1,
                )
            ),
            SizedBox(height: allWidth * 0.03),
            Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  '${feedsCount} источников',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: smallTextSize,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff808486),
                  ),
                  maxLines: 1,
                )
            )
          ]
      ),
      functionalBlockWidth: functionalBlockWidth,
      functionalBlock: functionalBlock,
      height: allWidth * 0.155,
      leftIconAllignment: Alignment(-0.4, -0.45),
    );
  }

  void deleteDialog(BuildContext context, int folderId, String folderName) {
    var allWidth = MediaQuery.of(context).size.width;

    showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Вы уверены что хотите удалить папку «$folderName»?'),
          content: Text('Источники из этой папки останутся в «Все подписки»'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('ОТМЕНИТЬ'),
            ),
            TextButton(
              onPressed: () {
                this.widget.contentBloc.add(DeleteUserFolder(folderId: folderId));
                Navigator.pop(context);
              },
              child: Text('ДА, УДАЛИТЬ'),
            ),
          ],
        )
    );
  }

  // TODO: Move to new class.
  void createOrEditDialog(BuildContext context,
      double allWidth,
      FolderDialogType type,
      FolderInfo? info) {

    showDialog<void>(
        context: context,
        builder: (context) => CreateEditFolderDialog(
            type,
            allWidth,
            info,
            (String text) {
              if (type == FolderDialogType.edit) {
                this.widget.contentBloc.add(ChangeUserFolder(
                    folderId: info?.id,
                    title: text,
                    iconName: info?.iconName,
                    isPublic: false)
                );
              } else {
                this.widget.contentBloc.add(CreateUserFolder(
                    title: text,
                    iconName: "folder",
                    isPublic: false)
                );
              }
              Navigator.pop(context);
            },
            () {
              Navigator.pop(context);
            }
        )
    );
  }

  void showTempSnackBar(BuildContext context) {
    final snackBar = SnackBar(
      behavior: SnackBarBehavior.floating,
      content: Text('Вот такие всплывашки скоро будут добавлены.'),
      action: SnackBarAction(
        label: 'Круто',
        onPressed: () {
          final snackBar2 = SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text('Да.'),
          );

          ScaffoldMessenger.of(context).showSnackBar(snackBar2);
        },
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    var headerTitle = 'Папки';
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;
    var bigTextSize = allWidth * 0.05;

    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            iconTheme: IconThemeData(
              color: Color(0xff151515),
            ),
            backgroundColor: Color(0xfff4f4f4),
            elevation: 1,
            title: Text(
              headerTitle,
              style: TextStyle(
                  color: Color(0xff151515),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Helvetica Neue',
                  fontStyle: FontStyle.normal,
                  fontSize: titleSize
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12),
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: (){
                  setState(() {
                    isEditing = !isEditing;
                  });
                },
                child: Text(
                  isEditing ? 'ГОТОВО  ' : 'ИЗМЕНИТЬ  ',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: allWidth * 0.033,
                    color: Color(0xff0a93a6),
                    fontWeight: FontWeight.w500,
                  ),
                ))
            ]
        ),
        body: BlocBuilder<ContentBloc, ContentState>(
            cubit: this.widget.contentBloc,
            builder: (BuildContext context, ContentState state) {
              if (state is FoldersBecome) {
                updateState(state.folders, state.allFeedsCount);
              } else if (state is FolderDeleted) {
                if (state.success) {
                  var folderId = state.folderId;
                  this.widget.items.removeWhere((element) => element.id == folderId);
                }
              } else if (state is FolderInfoUpdated) {
                var folderId = state.folder.id;
                var pos = this.widget.items.indexWhere((element) => element.id == folderId);
                this.widget.items[pos] = state.folder;
              } else if (state is FolderInfoCreated) {
                var folders = this.widget.items;
                folders.add(state.folder);
                updateState(folders, this._allFeedsCount);
              }

              return Column(
                children: [
                  InkWell(
                    onTap: () {
                      if (this.isEditing)
                        return;

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FolderScreen(
                                folderId: -1,
                                title: 'Все подписки',
                                iconData: Icons.folder_outlined,
                                newsListType: ListTypes.mainlenta,
                                feedsListType: FeedListTypes.alluserfeeds,
                              ),
                          ));
                    },
                    child: getLongButton(Icons.folder_outlined,
                      'Все подписки',
                      this._allFeedsCount,
                      isEditing ? 0 : allWidth * 0.155,
                      Visibility(
                        visible: !isEditing,
                        child: Container(
                            width: allWidth * 0.155,
                            child: Center(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                size: bigTextSize,
                                color: Color(0xffbbbfc0),
                              ),
                            )
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: allWidth * 0.027),
                  Expanded(
                    child: ListView.builder(
                      itemCount: this.widget.items.length,
                      itemBuilder: (context, index) {
                        FolderInfo item = this.widget.items.elementAt(index);
                        return InkWell(
                          onTap: () {
                            if (this.isEditing)
                              return;

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FolderScreen(
                                    folderId: item.id,
                                    title: item.title,
                                    iconData: Icons.folder_outlined,
                                  ),
                                ));
                          },
                          child: getLongButton(
                            Icons.folder_outlined,
                            item.title,
                            item.feedsCount,
                            isEditing ? allWidth * 0.31 : allWidth * 0.155,
                            Container(
                              width: isEditing ? allWidth * 0.31 : allWidth * 0.155,
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: isEditing,
                                    child: Container(
                                      width: allWidth * 0.155,
                                      child: GestureDetector(
                                        onTap: () {
                                          createOrEditDialog(
                                              context, allWidth,
                                              FolderDialogType.edit, item);
                                        },
                                        child: Center(
                                          child: Icon(
                                            Icons.edit,
                                            size: bigTextSize,
                                            color: Color(0xff0a93a6),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: isEditing,
                                    child: InkWell(
                                      onTap: () {
                                        this.deleteDialog(context, item.id, item.title);
                                      },
                                      child: Container(
                                        width: allWidth * 0.155,
                                        child: Center(
                                          child: Icon(
                                            Icons.delete_forever,
                                            size: bigTextSize,
                                            color: Color(0xff0a93a6),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: !isEditing,
                                    child: Container(
                                      width: allWidth * 0.155,
                                      child: Center(
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          size: bigTextSize,
                                          color: Color(0xffbbbfc0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        );
                      },
                    ),
                  ),
                  Visibility(
                    visible: this.isEditing,
                    child: Container(
                      height: allWidth * 0.155,
                      width: allWidth,
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(
                          color: Color(0xffE8E8E8),
                          width: 1.5,
                          ),
                        ),
                      ),
                      child: TextButton(
                        onPressed: () {
                          createOrEditDialog(context, bigTextSize,
                              FolderDialogType.create, null);
                        },
                        child: Text(
                          'СОЗДАТЬ ПАПКУ',
                          style: TextStyle(
                          fontFamily: 'Helvetica Neue',
                          fontSize: allWidth * 0.033,
                          color: Color(0xff0a93a6),
                          fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
        )
    );
  }
}
