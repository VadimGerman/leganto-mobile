
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/folder_info.dart';

enum FolderDialogType {
  create,
  edit,
}

typedef AcceptActionCallBack = void Function(String);
typedef DenyActionCallBack = void Function();

class CreateEditFolderDialog extends StatefulWidget {
  CreateEditFolderDialog(this._dialogType, this._allWidth,
      this._folderInfo, this._acceptActionCallBack, this._denyActionCallBack);

  final FolderDialogType _dialogType;
  final double _allWidth;
  final FolderInfo? _folderInfo;
  final AcceptActionCallBack _acceptActionCallBack;
  final DenyActionCallBack _denyActionCallBack;

  @override
  State<CreateEditFolderDialog> createState() => _CreateEditFolderDialogState();
}

class _CreateEditFolderDialogState extends State<CreateEditFolderDialog> {
  @override
  Widget build(BuildContext context) {
    final _folderTitleController = TextEditingController();
    var bigTextSize = this.widget._allWidth * 0.05;
    bool isEditDialog = this.widget._dialogType == FolderDialogType.edit;

    return AlertDialog(
      title: Text(isEditDialog ? 'Изменить папку' : 'Создать папку'),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              decoration: InputDecoration(
                filled: true,
                isDense: true,
              ),
              controller: _folderTitleController,
              // initialValue: title,                 // TODO: User _folderInfo.
              keyboardType: TextInputType.emailAddress,
              autocorrect: false,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Имя не должно быть пустым';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Выбрать иконку:'),
                InkWell(
                  onTap: () {
                    print('TODO: Icon selection.');
                  },
                  child: Icon(
                    Icons.folder_outlined,            // TODO: User _folderInfo.
                    size: bigTextSize*2,
                    color: Color(0xffbbbfc0),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            this.widget._denyActionCallBack();
          },
          child: Text('ОТМЕНИТЬ'),
        ),
        TextButton(
          onPressed: () {
            this.widget._acceptActionCallBack(_folderTitleController.text);
          },
          child: Text(isEditDialog ? 'СОХРАНИТЬ' : 'СОЗДАТЬ'),
        ),
      ],
    );
  }
}
