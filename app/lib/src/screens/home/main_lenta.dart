
import 'package:overone/src/screens/elements/paginated_load/paginated_item_base.dart';
import 'package:overone/src/screens/elements/paginated_load_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/news_list_data.dart';

class MainLenta extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainLentaState();
}

class _MainLentaState extends State<MainLenta>
    with SingleTickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    Tab(text: 'МОЯ ЛЕНТА'),
    Tab(text: 'РЕКОМЕНДАЦИИ'),
  ];

  final List<PaginatedLoadList> pages = <PaginatedLoadList>[
    PaginatedLoadList(item: PaginatedNewsItem(
        listInfo: NewsListData(
            type: ListTypes.mainlenta,
        )
    )),
    PaginatedLoadList(item: PaginatedNewsItem(
        listInfo: NewsListData(
            type: ListTypes.recomendations,
        )
    )),
  ];

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: tabs.length, initialIndex: 1);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var textSize = allWidth * 0.033;
    var logoSize = allWidth * 0.055;

    return Scaffold(
      // bottomNavigationBar: BottomNavigationBarWidget(),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xfff4f4f4),
        elevation: 1,
        title: Text('Overone',
          style: TextStyle(
            color: Color(0xff151515),
            fontWeight: FontWeight.w500,
            fontFamily: 'Helvetica Neue',
            fontStyle: FontStyle.normal,
            fontSize: logoSize
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        ),
        bottom: TabBar(
          indicatorColor: Color(0xff0A93A6),
          indicatorSize: TabBarIndicatorSize.label,
          labelColor: Color(0xff151515),
          unselectedLabelColor: Color(0xff808486),

          labelStyle: TextStyle(
            fontSize: textSize,
            fontWeight: FontWeight.w500,
            fontFamily: 'Helvetica Neue'),
          unselectedLabelStyle: TextStyle(
            fontSize: textSize,
            fontWeight: FontWeight.w500,
            fontFamily: 'Helvetica Neue'),
          controller: _tabController,
          tabs: tabs,
        ),
      ),
      body: Container(
        padding: EdgeInsets.zero,
        child: TabBarView(
          controller: _tabController,
          children: pages,
        ),
      ),
    );
  }
}
