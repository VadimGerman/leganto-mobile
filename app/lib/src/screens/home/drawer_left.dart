import 'package:overone/src/config/image_constants.dart';
import 'package:flutter/foundation.dart';
import 'package:overone/src/utils/app_state_notifier.dart';
import 'package:overone/src/widgets/cache_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared/modules/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:shared/modules/authentication/bloc/authentication/authentication_event.dart';
import 'package:shared/modules/authentication/bloc/bloc_controller.dart';
import 'package:shared/modules/authentication/models/current_user_data.dart';

class DrawerLeft extends StatelessWidget {
  DrawerLeft({required this.currentUserData});

  final CurrentUserData currentUserData;
  final AuthenticationBloc authenticationBloc =
      AuthenticationBlocController().authenticationBloc;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                          ),
                          child: CachedImage(
                            imageUrl: currentUserData.data.avatar,
                            fit: BoxFit.fitWidth,
                            errorWidget: Image.network(
                              AllImages().kDefaultImage,
                            ),
                            width: 70,
                            height: 70,
                            placeholder: CircularProgressIndicator(),
                          ),
                        ),
                        Container(
                          width: 30,
                          height: 30,
                        ),
                        Text('${currentUserData.data.login}',
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline3),
                      ],
                    ),
                    Switch(
                      value:
                      Provider
                          .of<AppStateNotifier>(context)
                          .isDarkMode,
                      onChanged: (value) {
                        Provider.of<AppStateNotifier>(context,
                            listen: false)
                            .updateTheme(value);
                      },
                    ),
                  ],
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: Theme
                  .of(context)
                  .dividerColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings',
                // TODO: Move to Theme.of(context).textTheme...
                style: TextStyle(color: Colors.black, fontSize: 18)
            ),
            onTap: () {
              // TODO: Open settings.
            },
          ),
          ListTile(
            leading: Icon(Icons.messenger_outline),
            title: Text('About',
                // TODO: Move to Theme.of(context).textTheme...
                style: TextStyle(color: Colors.black, fontSize: 18)
            ),
            onTap: () {
              // TODO: Open about.
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Logout',
                // TODO: Move to Theme.of(context).textTheme...
                style: TextStyle(color: Colors.black, fontSize: 18)
            ),
            onTap: () {
              authenticationBloc.add(UserLogOut());
            },
          ),
        ],
      ),
    );
  }


}
