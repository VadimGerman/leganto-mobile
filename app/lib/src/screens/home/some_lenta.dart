
import 'package:overone/src/screens/elements/paginated_load/paginated_item_base.dart';
import 'package:overone/src/screens/elements/paginated_load_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';

class SomeLenta extends StatefulWidget {
  const SomeLenta({
    required this.newsListInfo,
    required this.feedListInfo,
    this.folderId = -1,
  });

  final NewsListData newsListInfo;
  final FeedListData feedListInfo;
  final int folderId;

  @override
  State<SomeLenta> createState() => _SomeLentaState();
}

class _SomeLentaState extends State<SomeLenta> {
  PaginatedLoadList newsView() {
    return PaginatedLoadList(item: PaginatedNewsItem(
        listInfo: this.widget.newsListInfo
    ));
  }

  PaginatedLoadList feedsView() {
    return PaginatedLoadList(item: PaginatedFeedItem(
      listInfo: this.widget.feedListInfo,
      folderId: this.widget.folderId,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return this.widget.newsListInfo.type == ListTypes.none ?
          feedsView() : newsView();
  }
}
