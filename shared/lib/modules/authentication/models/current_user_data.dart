class CurrentUserData {
  Data data;

  CurrentUserData({this.data});

  CurrentUserData.fromJson(Map<String, dynamic> json) {
    var userInfo = json['userInfo'];
    data = userInfo != null ? new Data.fromJson(userInfo) : Data();
    //TODO: Feed list.
    // var feeds = userInfo['feeds'];
    // ad = json['ad'] != null ? new Ad.fromJson(json['ad']) : Ad();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['userInfo'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String email;
  String login;
  String lastName;
  String avatar;

  Data(
      {this.id,
      this.email = '',
      this.login = '',
      this.lastName = '',
      this.avatar = ''});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    // email = json['email'];
    login = json['login'];
    // lastName = json['last_name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    // data['email'] = this.email;
    data['login'] = this.login;
    // data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    return data;
  }
}
