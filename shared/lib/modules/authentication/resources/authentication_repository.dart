import 'package:api_sdk/main.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

class AuthenticationRepository {
  Future<dynamic> signUpWithEmailAndPassword(String email, String password) async {
    Map<String, String> data = {
      'login': email,
      'passwordHash': md5.convert(utf8.encode(password)).toString(),
      'avatar': ''
    };
    data = {'json': JsonEncoder().convert(data).toString()};
    final response = await ApiSdk.signUpWithEmailAndPassword(data);

    return response;
  }

  Future<dynamic> loginWithEmailAndPassword(String email, String password) async {
    Map<String, String> data = {
        'login': email,
        'passwordHash': md5.convert(utf8.encode(password)).toString()
    };
    data = {'json': JsonEncoder().convert(data).toString()};
    final response = await ApiSdk.loginWithEmailAndPassword(data);

    return response;
  }

  Future<dynamic> getUserInfo(int id, Map<String, String> headers) async {
    
    final response = await ApiSdk.getUserInfo(id, headers);

    return response;
  }

  Future<dynamic> getUserNews(int id, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getUserNews(id, headers, params);

    return response;
  }

  Future<dynamic> getCategories() async {

    final response = await ApiSdk.getCategories();

    return response;
  }

  Future<dynamic> getFeedNews(int feedId, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getFeedNews(feedId, headers, params);

    return response;
  }
}
