
class CategoryInfo {
  int id;
  String title;
  int categoriesCount;
  int feedsCount;

  CategoryInfo({this.id, this.title, this.categoriesCount, this.feedsCount});

  CategoryInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    categoriesCount = json['subcategoriesCount'] ?? -1;
    feedsCount = json['feedsCount'] ?? -1;
  }
}
