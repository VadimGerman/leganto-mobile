class FeedData {
  int id;
  String link;
  String title;
  String description;
  String imageUrl;
  String language;
  bool isSubscribed;

  FeedData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    link = json['link'];
    title = json['title'];
    description = json['description'];
    imageUrl = json['imageUrl'];
    language = json['language'];
    isSubscribed = json['isSubscribed'];
  }

  void reset(FeedData feed) {
    id = feed.id;
    link = feed.link;
    title = feed.title;
    description = feed.description;
    imageUrl = feed.imageUrl;
    language = feed.language;
    isSubscribed = feed.isSubscribed;
  }
}
