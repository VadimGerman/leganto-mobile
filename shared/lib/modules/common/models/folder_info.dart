
import 'dart:ui';

class FolderInfo {
  int id;
  String title;
  String iconName;
  int folderPosition;
  bool isPublic;
  int feedsCount;

  FolderInfo({this.id, this.title, this.iconName,
    this.folderPosition, this.isPublic, this.feedsCount});

  FolderInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    iconName = json['iconName'];
    folderPosition = json['folderPosition'];
    isPublic = json['isPublic'];
    feedsCount = json['feedsCount'];
  }

  @override
  operator ==(Object other) {
    if (identical(this, other))
      return true;

    if (other.runtimeType != runtimeType)
      return false;

    return other is FolderInfo &&
      other.id == this.id &&
      other.title == this.title &&
      other.iconName == this.iconName &&
      other.folderPosition == this.folderPosition &&
      other.isPublic == this.isPublic &&
      other.feedsCount == this.feedsCount;
  }

  @override
  int get hashCode => hashValues(id, title, iconName, folderPosition, isPublic, feedsCount);
}
