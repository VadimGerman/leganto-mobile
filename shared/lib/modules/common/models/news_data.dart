class NewsData {
  int id;
  int feedId;
  String guid;
  String title;
  String link;
  String description;
  double putDate;
  String enclosure;
  String source;
  String imageUrl;
  String category;

  NewsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    feedId = json['feedId'];
    guid = json['guid'];
    title = json['title'];
    link = json['link'];
    description = json['description'];
    putDate = json['putDate'];
    enclosure = json['enclosure'];
    source = json['source'];
    imageUrl = json['imageUrl'];
    category = json['category'];

    imageUrl = imageUrl ?? "";
  }
}
